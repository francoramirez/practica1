import random
words = ["python", "programación", "computadora", "código", "desarrollo",
"inteligencia"]

# Elegir una palabra al azar
secret_word = random.choice(words)

# Número máximo de intentos fallados
max_attempts = 10
cont_fails = 0

# Lista para almacenar las letras adivinadas
guessed_letters = []


print("¡Bienvenido al juego de adivinanzas!")

while True:                                                                                             #SE ASEGURA EL INGRESO DE UNA DE LAS 3 DIFICULTADES
    difficult = input("Ingrese el nivel de dificultad que desea:\n 1. Facil\n 2. Media\n 3. Dificil\n")
    difficult = int(difficult)
    if difficult not in [1,2,3]:
        print("Ingrese un numero valido de dificultad")
        continue
    else:
        print(f"Muy bien, su nivel de dificultad seleccionada es el {difficult}")
        break

print("Estoy pensando en una palabra. ¿Puedes adivinar cuál es?")
letters=[]
shown_letters= []
match difficult:
    case 1: 
        vocales="aeiouáéíóú"
        for letter_case1 in secret_word:                    #RECORRE LA PALABRA A ADVINIAR BUSCANDO LAS VOCALES Y ASIGNANDOLAS PARA MOSTRAR LUEGO EN PANTALLA
             if letter_case1 in vocales:
                 letters.append(letter_case1)
                 shown_letters.append(letter_case1)
                 guessed_letters.append(letter_case1)
             else:
                 letters.append("_")
        word_displayed= "".join(letters)
    case 2:                                                 #ASIGNAMOS LA PRIMERA Y LA ULTIMA LETRA DE LA PALABRA
        for i in range(len(secret_word)):
            letters.append("_")
        letters[0] = secret_word[0]
        shown_letters.append(secret_word[0])
        shown_letters.append(secret_word[-1])
        letters[-1] = secret_word[-1]
        word_displayed = "".join(letters)
        
    case 3:  
        word_displayed = "_" * len(secret_word) 
                            

print(f"Palabra: {word_displayed}")

while cont_fails < max_attempts: #10 intentos
    
 while True:                            #BUCLE PARA CONFIRMAR QUE LA LETRA INGRESADA NO SEA VACIO
    letter = input("Ingresa una letra: ").lower()
    if letter == "":
        print("Por favor, ingrese una letra")   
        continue
    else:
        break
        
 # Verificar si la letra ya ha sido adivinada

 
 if letter in guessed_letters: 
     if difficult == 1:
         if letter in shown_letters:
             print("Esta letra ya esta siendo mostrada en pantalla la cantidad de veces que se repite")
         else:          
             print("Ya has intentado con esa letra. Intenta con otra.")
             cont_fails += 1
     else:          
        print("Ya has intentado con esa letra. Intenta con otra.")
        cont_fails += 1
     continue                                                       
 
 # Agregar la letra a la lista de letras adivinadas
 guessed_letters.append(letter)
 
 # Verificar si la letra está en la palabra secreta
 
 if letter in secret_word:
     if difficult == 2:
        if letter in shown_letters:
            secret_word_list = list(secret_word)
            secret_word_list[0] = "_"
            secret_word_list[-1] = "_"
            if letter in secret_word_list:
                print(f"En la palabra secreta se encuentra mas de 1 {letter}, Bien hecho")
                guessed_letters.append(letter)
            else:
                print(f"Lo siento, solo hay una {letter} en esta palabra")
                cont_fails += 1
     else: 
       print("¡Bien hecho! La letra está en la palabra.")
 else:
     print("Lo siento, la letra no está en la palabra.")
     cont_fails += 1
     
 # Mostrar la palabra parcialmente adivinada
 letters = []
 
 for letter in secret_word:
     if (letter in guessed_letters):                                  #PREGUNTA QUE LETRAS HE ADIVINADO
         letters.append(letter)
     else:
         letters.append("_")
 if difficult==2 :                                                      #En caso de elegir dificultad 2 muestra en pantalla la primera y ultima letra
     letters[0]=shown_letters[0]
     letters[-1]=shown_letters[-1]
 word_displayed = "".join(letters)
 print(f"Palabra: {word_displayed}")
 
 # Verificar si se ha adivinado la palabra completa
 if word_displayed == secret_word:
     print(f"¡Felicidades! Has adivinado la palabra secreta: {secret_word}")
     break
else:                                                                           #EN CASO QUE EL BUCLE NO HAYA SIDO DENETINO POR EL BREAK SE EJECUTARA ESTE ELSE
     print(f"¡Oh no! Has fallado {max_attempts} intentos.")
     print(f"La palabra secreta era: {secret_word}")

